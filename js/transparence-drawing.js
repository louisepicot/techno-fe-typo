
Physijs.scripts.worker = '../librairies/physijs_worker.js'
Physijs.scripts.ammo = '../librairies/ammo.js';

let scene = new Physijs.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    spotLight = new THREE.SpotLight(0xFF0000),
    camera,
    ground,
    box,
    controls;


const init = () => {
    scene.setGravity(new THREE.Vector3(-0, -50, -10))
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xffffff, 0);
    renderer.shadowMap.enabled = true;
    document.getElementById('three-container').appendChild(renderer.domElement);
    // scene.add(light)



    spotLight.position.set(-30, 100, 0);
    scene.add(spotLight);
    scene.add(light);

    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 1000)
    camera.position.set(300, 200, 100);
    camera.lookAt(scene.position)
    scene.add(camera)

    let boxMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({
            color: 0xFF0000,
            ambient: 0x0088aa,
            specular: 0x003344,
            transparent: true,
            opacity: 0.3,
            shininess: 100
        }),
        0,
        0.8
    )

    box = new Physijs.BoxMesh(
        new THREE.CubeGeometry(15, 15, 15),
        boxMaterial,
    )

    box.name = "box";
    scene.add(box)


    let boxMaterial2 = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({
            color: 0xFF0000,
            ambient: 0x0088aa,
            specular: 0x003344,
            transparency: true,
            opacity: 0.3,
            shininess: 100
        }),
        0,
        0.8
    )

    box2 = new Physijs.BoxMesh(
        new THREE.CubeGeometry(5, 15, 5),
        boxMaterial2,
    )


    box2.name = "box";
    scene.add(box2)




    let groundMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture('../assets/images/dessins-small/alice057.jpg'),
        }),
        0,
        0.6
    )
    ground = new Physijs.BoxMesh(
        new THREE.CubeGeometry(150, 5, 150),
        groundMaterial,
        0
    );

    ground.name = 'ground';
    // ground.position.y = -25;
    scene.add(ground)

  

    box.addEventListener('collision', function (
        otherObject,
        relativeVelocity,
        relativeRotation,
        contactNormal) {

        if (otherObject.name == "ground") {
            // alert("hit the ground")
            console.log("otherObject", otherObject)
        }
    }
    )

    controls = new THREE.OrbitControls(camera)
    controls.minDistance = 100;
    controls.maxDistance = 600;
    controls.addEventListener('change', render)

    render();

}

// function onDocumentMouseMove(event) {
//     // Manually fire the event in OrbitControls

//     controls.handleMouseMoveRotate(event);


// }

const render = () => {
    // scene.simulate();
    renderer.render(scene, camera);
    requestAnimationFrame(render);

}





window.onload = init();



