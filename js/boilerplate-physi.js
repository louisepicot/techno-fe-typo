

Physijs.scripts.worker = '../librairies/physijs_worker.js'
Physijs.scripts.ammo = '../librairies/ammo.js';

let scene = new Physijs.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer() : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    ground,
    box,
    controls;

const init = () => {
    scene.setGravity(new THREE.Vector3(-0, -50, -10))
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.getElementById('three-container').appendChild(renderer.domElement);
    scene.add(light)

    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 1000)
    camera.position.set(100, 100, 100);
    camera.lookAt(scene.position)
    scene.add(camera)

    let boxMaterial = Physijs.createMaterial(
        new THREE.MeshBasicMaterial({
            color: 0xFF0000
        }),
        0,
        0.8
    )

    box = new Physijs.BoxMesh(
        new THREE.CubeGeometry(15, 15, 15),
        boxMaterial,
    )

    box.rotation.y = 40;
    box.rotation.x = 90;
    box.rotation.z = 60;
    box.name = "box";
    scene.add(box)




    let groundMaterial = Physijs.createMaterial(
        new THREE.MeshBasicMaterial({
            color: 0x008888,
        }),
        0,
        0.6
    )
    ground = new Physijs.BoxMesh(
        new THREE.CubeGeometry(150, 5, 150),
        groundMaterial,
        0
    );

    ground.name = 'ground';
    ground.position.y = -25;
    scene.add(ground)

    box.addEventListener('collision', function (
        otherObject,
        relativeVelocity,
        relativeRotation,
        contactNormal) {

        if (otherObject.name == "ground") {
            // alert("hit the ground")
            console.log("otherObject", otherObject)
        }
    }
    )

   controls = new THREE.OrbitControls(camera)
   controls.addEventListener('change', render)

    render();

}

const render = () => {
    scene.simulate();

    renderer.render(scene, camera);
    requestAnimationFrame(render);

}





window.onload = init();



