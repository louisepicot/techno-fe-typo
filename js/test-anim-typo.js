


let scene = new THREE.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    ground,
    box,
    controls,
    geometry,
    materials,
    group,
    loader = new THREE.FontLoader();

var targetRotation = 0;
var targetRotationOnMouseDown = 0;
var mouseX = 0;
var mouseXOnMouseDown = 0;
var windowHalfX = window.innerWidth / 2;

init();
animate();


function init() {

    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xffffff, 0);
    document.getElementById('three-container').appendChild(renderer.domElement);
    scene.add(light);

    const lightDir = new THREE.DirectionalLight(0xff2d00);
    lightDir.position.set(0, 5, 100);
    lightDir.castShadow = true;
    scene.add(lightDir)

    camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 1500);
    camera.position.set(0, 400, 700);
    cameraTarget = new THREE.Vector3(0, 150, 0);
    scene.add(camera)


    materials = [
        new THREE.MeshPhongMaterial({ color: 0xffffff, flatShading: true }), // front
        new THREE.MeshPhongMaterial({ color: 0xffffff }) // side
    ];

    group = new THREE.Group();
    group.position.y = 100;
    scene.add(group);


    material = new THREE.MeshPhongMaterial({
        color: 0x0088aa,
        specular: 0xff2d00,
        shininess: 100,
        flatShading: THREE.FlatShading,
        side: THREE.DoubleSide
    });



    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('touchstart', onDocumentTouchStart, false);
    document.addEventListener('touchmove', onDocumentTouchMove, false);

    controls = new THREE.OrbitControls(camera)
    controls.maxZoom = 700
    controls.addEventListener('change', render)


    loadFont()
    render();

}


function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    // scene.simulate();
    // text.rotation.x += 0.5;

    group.rotation.y += (targetRotation - group.rotation.y) * 0.05;
    camera.lookAt(cameraTarget);
    renderer.clear();
    renderer.render(scene, camera);

}


function loadFont() {
    var loader = new THREE.FontLoader();
    loader.load('./assets/fonts/H2C_Regular.json', function (res) {
        font = res;
        createText();
    });
}
function createText() {
   let textgeo = new THREE.TextGeometry('TECHNOFE', {
        font: font,
        height: 20,
        size: 70,
        hover: 30,
        curveSegments: 4,
        bevelThickness: 2,
        bevelSize: 1.5,
        bevelEnabled: true,
    })

    textgeo.computeBoundingBox();
    textgeo.computeVertexNormals();

    var centerOffset = - 0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);

    textGeo = new THREE.BufferGeometry().fromGeometry(textGeo)
    textMesh = new THREE.Mesh(textGeo, materials);
    textMesh.position.x = centerOffset;
    textMesh.position.y = hover;
    textMesh.position.z = 0;
    textMesh.rotation.x = 0;
    textMesh.rotation.y = Math.PI * 2;
    group.add(textMesh)

}


function onDocumentMouseDown(event) {
    event.preventDefault();
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);
    document.addEventListener('mouseout', onDocumentMouseOut, false);
    mouseXOnMouseDown = event.clientX - windowHalfX;
    targetRotationOnMouseDown = targetRotation;
}


function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX;
    targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.02;
}
function onDocumentMouseUp() {
    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
}
function onDocumentMouseOut() {
    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
}
function onDocumentTouchStart(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        mouseXOnMouseDown = event.touches[0].pageX - windowHalfX;
        targetRotationOnMouseDown = targetRotation;
    }
}
function onDocumentTouchMove(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.05;
    }
}


window.onload = init();



