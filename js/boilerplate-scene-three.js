


let scene = new THREE.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    box,
    controls,
    boxMaterial,
    geometry;

function init() {

    renderer.setSize(window.innerWidth , window.innerHeight);
    renderer.setClearColor(0x000000, 1);
    document.getElementById('three-container').appendChild(renderer.domElement);
    scene.add(light);

    // const lightDir = new THREE.DirectionalLight(0xff2d00);
    // lightDir.position.set(0, 5, 100);
    // scene.add(lightDir)

    camera = new THREE.PerspectiveCamera(30, window.innerWidth  / window.innerHeight, 1, 1500);
    camera.position.set(0, 8, 1);
    camera.lookAt(scene.position)
    scene.add(camera)

    boxMaterial = new THREE.MeshBasicMaterial({color: 0xFF0000})

    geometry = new THREE.BoxGeometry(1, 1, 1);
    box = new THREE.Mesh(geometry, boxMaterial);
    box.name ="box"
    scene.add(box);
   
    // controls = new THREE.OrbitControls(camera)
    // // controls.maxZoom = 700
    // controls.addEventListener('change', render)

    render();

}

function render() {
    box.rotation.y += 0.01
    box.rotation.x += 0.01
   
    renderer.render(scene, camera);
    requestAnimationFrame(render);

}



window.onload = init();



