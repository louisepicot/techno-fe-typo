


let scene = new THREE.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    ground,
    box,
    controls,
    geometry,
    material,
    yesNo = true,
    loader = new THREE.FontLoader();


const init = () => {

    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xffffff, 0);
    document.getElementById('three-container').appendChild(renderer.domElement);
    scene.add(light);

    const lightDir = new THREE.DirectionalLight(0xff2d00);
    lightDir.position.set(0, 5, 100);
    lightDir.castShadow = true;
    scene.add(lightDir)
    const lightDirBack = new THREE.DirectionalLight(0xff2d00);
    lightDirBack.position.set(0, 5, -100);
    lightDirBack.castShadow = true;
    scene.add(lightDirBack)

    const lightDirRight = new THREE.DirectionalLight(0xff2d00);
    lightDirRight.position.set(-50, 25, 0);
    lightDirRight.castShadow = true;
    scene.add(lightDirRight)

    const lightDirLeft = new THREE.DirectionalLight(0xff2d00);
    lightDirLeft.position.set(50, -25, 0);
    lightDirLeft.castShadow = true;
    scene.add(lightDirLeft)

    camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 1500);
    camera.position.set(0, 0, 700);
    camera.lookAt(scene.position)
    scene.add(camera)



    material = new THREE.MeshPhongMaterial({
        color: 0x0088aa,
        specular: 0xff2d00,
        shininess: 100,
        flatShading: THREE.FlatShading,
        side: THREE.DoubleSide 
    });


    controls = new THREE.OrbitControls(camera)
    controls.dispose();
    controls.update();
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('click', onDocumentClick, false);

    // controls.maxDistance = 700
    // controls.minDistance = 700
    // controls.rotateSpeed = 1
    // controls.addEventListener('change', render)

    loadFont()
    render();

}

function onDocumentMouseMove(event) {
    // Manually fire the event in OrbitControls

        controls.handleMouseMoveRotate(event);
 
   
}

function onDocumentClick(event) {

  
    // Manually fire the event in OrbitControls
}

const render = () => {
    // scene.simulate();
    // text.rotation.x += 0.5;
    // spinCamera()
    controls.update();
    renderer.render(scene, camera);
    requestAnimationFrame(render);
}

// var rotation = 0
// function spinCamera() {
//     rotation += 0.010
//     // camera.position.z = Math.sin(rotation) * 80;
//     camera.position.x = Math.cos(rotation) * 80;
//     camera.lookAt(scene.position)
// }
function loadFont() {
    var loader = new THREE.FontLoader();
    loader.load('./assets/fonts/H2C_Regular.json', function (res) {
        font = res;
        createText();
    });
}
function createText() {
   var textgeo = new THREE.TextGeometry('TECHNO FEMMES', {
        font: font,
        size: 50,
        height: 10,
        curveSegments: 12,
        weight: "bold",
        bevelThickness: 1,
        bevelSize: 1,
        bevelEnabled: true
    })

    textgeo.computeBoundingBox();
   textgeo.computeVertexNormals();

    var text = new THREE.Mesh(textgeo, material)
    text.position.x = -textgeo.boundingBox.max.x / 2;
    text.castShadow = true;
   scene.add(text)
}



window.onload = init();



